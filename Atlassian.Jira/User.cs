﻿using Atlassian.Jira.Remote;

namespace Atlassian.Jira
{
	public class User : JiraEntity
	{
		private readonly RemoteUser _remoteUser;

		internal User(RemoteUser remoteUser)
			: base(remoteUser)
		{
			_remoteUser = remoteUser;
		}

		internal RemoteUser RemoteUser
		{
			get
			{
				return _remoteUser;
			}
		}

		public string Email
		{
			get
			{
				return _remoteUser.email;
			}
		}

		public string FullName
		{
			get
			{
				return _remoteUser.fullname;
			}
		}

		public string Name
		{
			get
			{
				return _remoteUser.name;
			}
		}
	}
}