﻿using System.Globalization;

using Atlassian.Jira.Remote;

namespace Atlassian.Jira
{
	public class Role
	{
		internal Role(RemoteProjectRole role)
		{
			Description = role.description;
			Name = role.name;
			Id = role.id.Value.ToString(CultureInfo.InvariantCulture);
		}

		public string Description
		{
			get;
			private set;
		}

		public string Name
		{
			get;
			private set;
		}

		public string Id
		{
			get;
			private set;
		}
	}
}